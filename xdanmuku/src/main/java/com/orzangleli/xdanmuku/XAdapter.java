package com.orzangleli.xdanmuku;


import ohos.agp.components.Component;

import java.util.HashMap;
import java.util.Stack;

/**
 * Created by Administrator on 2017/4/17.
 */

public abstract class XAdapter<M>{

    private HashMap<Integer,Stack<Component>> cacheViews ;

    public XAdapter()
    {
        cacheViews = new HashMap<>();
        int typeArray[] = getViewTypeArray();
        for(int i=0;i<typeArray.length;i++){
            Stack<Component> stack = new Stack<>();
            cacheViews.put(typeArray[i],stack);
        }
    }

    public abstract Component getView(M danmuEntity, Component convertView);

    public abstract int[] getViewTypeArray();
    public abstract int getSingleLineHeight();

    synchronized public void addToCacheViews(int type,Component view) {
        if(cacheViews.containsKey(type)){
            cacheViews.get(type).push(view);
        }
        else{
            throw new Error("you are trying to add undefined type view to cacheViews,please define the type in the XAdapter!");
        }
    }

    synchronized public Component removeFromCacheViews(int type) {
        if(cacheViews.get(type).size()>0)
            return cacheViews.get(type).pop();
        else
            return null;
    }

    //缩小缓存数组的长度,以减少内存占用
    synchronized public void shrinkCacheSize() {
        int typeArray[] = getViewTypeArray();
        for(int i=0;i<typeArray.length;i++){
            int type = typeArray[i];
            Stack<Component> typeStack = cacheViews.get(type);
            int length = typeStack.size();
            while(typeStack.size() > ((int)(length/2.0+0.5))){
                typeStack.pop();
            }
            cacheViews.put(type,typeStack);
        }
    }

    public int getCacheSize()
    {
        int totalSize = 0;
        int typeArray[] = getViewTypeArray();
        Stack typeStack = null;
        for(int i=0;i<typeArray.length;i++){
            int type = typeArray[i];
            typeStack = cacheViews.get(type);
            totalSize += typeStack.size();
        }
        return totalSize;
    }


}
