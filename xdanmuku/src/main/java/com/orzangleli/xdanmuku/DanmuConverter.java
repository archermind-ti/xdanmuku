package com.orzangleli.xdanmuku;


import ohos.agp.components.Component;

/**
 * Created by Administrator on 2017/3/30.
 */

public abstract class DanmuConverter<M>{
    public abstract int getSingleLineHeight();
    public abstract Component convert(M model);
}
