package com.orzangleli.xdanmuku;

import ohos.agp.animation.Animator;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Handler;

/**
 */

public class DanmuContainerView extends ComponentContainer implements VideoProgoressCallback, Component.EstimateSizeListener, ComponentContainer.ArrangeListener {

    private final int DANMU_STEP = 500; // 0.5秒
    private long mLastDanmuTime;

    public final static int LOW_SPEED = 10*1000;
    public final static int NORMAL_SPEED = 4*1000;
    public final static int HIGH_SPEED = 1000;

    public final static int GRAVITY_TOP = 1 ;    //001
    public final static int GRAVITY_CENTER = 2 ;  //010
    public final static int GRAVITY_BOTTOM = 4 ;  //100
    public final static int GRAVITY_FULL = 7 ;   //111

    private int gravity = 7;


    private int spanCount = 8;

    private int WIDTH, HEIGHT;

    public List<Component> spanList;

    private int singleLineHeight;

    XAdapter xAdapter;

    int speed = LOW_SPEED;

    private List<Model> mCachedModelPool;
    private ShowEventHandler mHandler;


    public DanmuContainerView(Context context) {
        this(context, null, 0);
    }

    public DanmuContainerView(Context context, AttrSet attrs) {
        this(context, attrs, 0);
    }

    public DanmuContainerView(Context context, AttrSet attrs, int defStyle) {
        super(context, attrs, defStyle+"");
        spanList = new ArrayList<Component>();
        mCachedModelPool = new ArrayList<>();
        mHandler=new ShowEventHandler(EventRunner.current());
        setEstimateSizeListener(this);
        setArrangeListener(this);
    }

    public void addDanmuIntoCachePool(List<Model> tmp) {
        mCachedModelPool.addAll(tmp);
    }


    public void setGravity(int gravity) {
        this.gravity = gravity;
    }

    OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener listener){
        onItemClickListener = listener;
    }

    public void setAdapter(XAdapter danmuAdapter) {
        xAdapter = danmuAdapter;
        singleLineHeight = danmuAdapter.getSingleLineHeight();
        new Thread(new MyRunnable()).start();
    }

    @Override
    public void onProgress(long time) {
        if (mCachedModelPool == null || mCachedModelPool.size() == 0) {
            return ;
        }
        // 显示time 至 time + DANMU_STEP 之间的弹幕
        for (int i =0;i<mCachedModelPool.size();i++) {
            Model model = mCachedModelPool.get(i);
            if (model != null && model.getShowTime() >= time && model.getShowTime() < time + DANMU_STEP && mLastDanmuTime < time) {
                addDanmu(model);
                mLastDanmuTime = time + DANMU_STEP;
            }
        }
    }


    public void resetDanmuProgress() {
        mLastDanmuTime = 0;
    }

    @Override
    public boolean onArrange(int i, int i1, int i2, int i3) {
        return false;
    }

    //单项点击监听器
    public interface OnItemClickListener{
        void onItemClick(Model model);
    }


    /**
     * 弹幕移动速度
     * 建议使用 DanmuContainerView.LOW_SPEED, DanmuContainerView.NORMAL_SPEED, DanmuContainerView.HIGH_SPEED
     * 自定义速度从1 到 8之间，值越大速度越快
     */
    public void setSpeed(int s) {
        speed = s;
    }

    @Override
    public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {
        int width = getEstimatedWidth();
        int height = getEstimatedHeight();//MeasureSpec.getSize(heightMeasureSpec);
        WIDTH = width;
        HEIGHT = height;

        spanCount = HEIGHT / singleLineHeight;
        for (int i = 0; i < this.spanCount; i++) {
            if (spanList.size() <= spanCount)
                spanList.add(i, null);
        }
        return false;
    }

    public void addDanmu(final Model model){
        if (xAdapter == null) {
            throw new Error("XAdapter(an interface need to be implemented) can't be null,you should call setAdapter firstly");
        }

        Component danmuView = null;
        if(xAdapter.getCacheSize() >= 1){
            danmuView = xAdapter.getView(model,xAdapter.removeFromCacheViews(model.getType()));
            if(danmuView == null)
                addTypeView(model,danmuView,false);
            else
                addTypeView(model,danmuView,true);
        }
        else {
            danmuView = xAdapter.getView(model,null);
            addTypeView(model,danmuView,false);
        }

        //添加监听
        danmuView.setClickedListener(new ClickedListener() {
            @Override
            public void onClick(Component component) {
                if(onItemClickListener != null)
                    onItemClickListener.onItemClick(model);
            }
        });

    }


    public void addTypeView(Model model,Component child,boolean isReused) {
        addComponent(child);
        child.estimateSize(0, 0);
        //把宽高拿到，宽高都是包含ItemDecorate的尺寸
        int width = child.getEstimatedWidth();
        int height = child.getEstimatedHeight();
        //获取最佳行数
        int bestLine = getBestLine();
        child.arrange(WIDTH, singleLineHeight * bestLine, WIDTH + width, singleLineHeight * bestLine + height);

        InnerEntity innerEntity = null;
//        innerEntity = (InnerEntity) child.getTag();
//        if(!isReused || innerEntity==null){
            innerEntity = new InnerEntity();
//        }
        innerEntity.model = model;
        innerEntity.bestLine = bestLine;
        child.setTag(innerEntity);

        if(spanList.size()==0){
            spanList.add(child);
        }else {
            if(bestLine<spanList.size()){
                spanList.set(bestLine, child);
            }else {
                spanList.add(child);
            }
        }

    }



    private int getBestLine() {
        //转换成2进制
        int gewei = gravity % 2;   //个位是
        int temp = gravity / 2;
        int shiwei = temp % 2;
        temp = temp / 2;
        int baiwei = temp % 2;

        //将所有的行分为三份,前两份行数相同,将第一份的行数四舍五入
        int firstPart = (int)(spanCount / 3.0f + 0.5f);

        //构造允许输入行的列表
        List<Integer> legalLines = new ArrayList<>();
        if(gewei == 1){
            for(int i=0;i<firstPart;i++)
                legalLines.add(i);
        }
        if(shiwei == 1){
            for(int i=firstPart;i<2*firstPart;i++)
                legalLines.add(i);
        }
        if(baiwei == 1){
            for(int i=2*firstPart;i<spanCount;i++)
                legalLines.add(i);
        }


        int bestLine = 0;
        //如果有空行直接结束
        for (int i = 0; i < spanCount; i++) {
            if (spanList.get(i) == null) {
                bestLine = i;
                if(legalLines.contains(bestLine))
                    return bestLine;
            }
        }
        float minSpace = Integer.MAX_VALUE;
        //没有空行，就找最大空间的
        for (int i = spanCount - 1; i >= 0; i--) {
            if(legalLines.contains(i)) {
                if (spanList.get(i).getPivotX() + spanList.get(i).getWidth() <= minSpace) {
                    minSpace = spanList.get(i).getPivotX() + spanList.get(i).getWidth();
                    bestLine = i;
                }
            }
        }
        return bestLine;
    }

    class InnerEntity{
        public int bestLine;
        public Model model;
    }


    private class MyRunnable implements Runnable {
        @Override
        public void run() {
            int count = 0;
            while(true){
                if(count < 7500){
                    count ++;
                }
                else{
                    count = 0;
                    if(DanmuContainerView.this.getChildCount() < xAdapter.getCacheSize() / 2){
                        xAdapter.shrinkCacheSize();
                        System.gc();
                    }
                }
                if(DanmuContainerView.this.getChildCount() >= 0){
                    mHandler.sendEvent(1);
                }

                try {
                    Thread.sleep(16);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }
    }
    private class ShowEventHandler extends EventHandler {

        public ShowEventHandler(EventRunner runner) throws IllegalArgumentException {
            super(runner);
        }

        @Override
        protected void processEvent(InnerEvent event) {
            super.processEvent(event);
            if (event == null) {
                return;
            }
            if(event.eventId==1)
            {
                for(int i=0;i<DanmuContainerView.this.getChildCount();i++){
                    Component view = DanmuContainerView.this.getComponentAt(i);
                    if(view.getContentPositionX()+view.getWidth() >= 0) {
                        view.createAnimatorProperty().moveFromX(view.getContentPositionX()).moveToX(0)
                                .setCurveType(Animator.CurveType.OVERSHOOT)
                                .rotate(0)
                                .setDuration(speed)
                                .setStateChangedListener(new Animator.StateChangedListener() {
                                    @Override
                                    public void onStart(Animator animator) {
                                        System.out.println("Component onStart");
                                    }

                                    @Override
                                    public void onStop(Animator animator) {
                                        System.out.println("Component onStop");
                                    }

                                    @Override
                                    public void onCancel(Animator animator) {
                                        System.out.println("Component onCancel");

                                    }

                                    @Override
                                    public void onEnd(Animator animator) {
                                        System.out.println("Component onEnd");
//                                        view.setVisibility(INVISIBLE);
                                        DanmuContainerView.this.removeComponent(view);
                                    }

                                    @Override
                                    public void onPause(Animator animator) {
                                        System.out.println("Component onPause");

                                    }

                                    @Override
                                    public void onResume(Animator animator) {
                                        System.out.println("Component onResume");

                                    }
                                })
                                .start();
                    }else{
                        //添加到缓存中
                        int type = ((InnerEntity)view.getTag()).model.getType();
                        xAdapter.addToCacheViews(type,view);
                        DanmuContainerView.this.removeComponent(view);
                    }
                }
            }

        }

    }
}
