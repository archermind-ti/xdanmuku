# XDanmuku

#### 简介
一种支持多种弹幕样式的弹幕视图控件

#### 功能
本项目是一个开源的弹幕控件库，能够支持多种样式弹幕，弹幕点击监听，弹幕分区域显示，自定义移动速度等功能，项目原理是通过自定义ComponentContainer。可能是目前轻量级弹幕库中功能最强大的一款了。

#### 演示
![demo预览](https://gitee.com/archermind-ti/xdanmuku/raw/master/gif.gif "在这里输入图片标题")

#### 集成

1.  下载源码工程使用
```
...
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    testImplementation 'junit:junit:4.13'
    ohosTestImplementation 'com.huawei.ohos.testkit:runner:1.0.0.100'
    compile project(path: ':xdanmuku')
    implementation 'io.openharmony.tpc.thirdlib:glide:1.0.6'
}
...
```
2.maven引用
```
...
allprojects {
    repositories {
     mavenCentral()
    }
}
...
dependencies {
 ...
 implementation 'com.gitee.archermind-ti:XDanmuku:1.0.0'
 ...
}
```
#### 使用说明

1. 添加控件
```
    <com.orzangleli.xdanmuku.DanmuContainerView
        ohos:id="$+id:danmuContainerView"
        ohos:width="match_parent"
        ohos:height="240vp"
        />

```
2. 添加自定义弹幕Entity（需要继承自Model）
类似于[DanmuEntity.java](https://gitee.com/archermind-ti/xdanmuku/raw/master/entry/src/main/java/com/orzangleli/recyclerviewdanmu/DanmuEntity.java)

3. 继承XAdapter
类似于ListContainer的BaseItemProvider的结构，具体参照[DanmuAdapter.java](https://gitee.com/archermind-ti/xdanmuku/raw/master/entry/src/main/java/com/orzangleli/recyclerviewdanmu/DanmuAdapter.java)

4. 添加弹幕
```
 DanmuEntity danmuEntity = new DanmuEntity();
                    danmuEntity.setContent("弹幕"+i);
                    danmuEntity.setType(0);
                    danmuEntity.setTime("23:20:11");
                    danmuEntity.setShowTime((long) (VIDEO_DURATION * random.nextFloat()));
                    danmuEntities.add(danmuEntity);
```

5. 弹幕点击事件监听
```
//弹幕点击事件
        danmuContainerView.setOnItemClickListener(new DanmuContainerView.OnItemClickListener() {
            @Override
            public void onItemClick(Model model) {
                DanmuEntity danmuEntity = (DanmuEntity) model;
                //弹框提示会造成卡顿
                new ToastDialog(MainAbility.this).setContentText( danmuEntity.content).show();
            }
        });

```
6. 设置弹幕移动速度
```
DanmuContainerView中预设了三种弹幕移动速度：

    public final static int LOW_SPEED = 10*1000;
    public final static int NORMAL_SPEED = 4*1000;
    public final static int HIGH_SPEED = 1000;
```
```
设置速度通过setSpeed方法：
danmuContainerView.setSpeed(DanmuContainerView.HIGH_SPEED);

```

7. 弹幕显示区域
```
//只在上方和中间区域显示弹幕
danmuContainerView.setGravity(DanmuContainerView.GRAVITY_TOP | DanmuContainerView.GRAVITY_CENTER);


```

#### 编译说明
1. 将项目通过git clone 至本地
2. 使用DevEco Studio 打开该项目，然后等待Gradle 构建完成
3. 点击Run运行即可（真机运行可能需要配置签名）


#### 版本迭代
## 1.0.0
1.完成基础功能，弹幕显示。

[changelog](https://gitee.com/archermind-ti/xdanmuku/raw/master/changelog.md)

#### MIT License

Copyright (c) 2017 orzangleli

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.