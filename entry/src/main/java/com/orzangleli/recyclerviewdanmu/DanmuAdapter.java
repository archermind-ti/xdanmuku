package com.orzangleli.recyclerviewdanmu;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.orzangleli.xdanmuku.XAdapter;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.utils.net.Uri;

import java.util.Random;

/**
 * Created by Administrator on 2017/4/17.
 */

public class DanmuAdapter extends XAdapter<DanmuEntity> {

    final int ICON_RESOURCES[] = {ResourceTable.Media_icon1,ResourceTable.Media_icon2,ResourceTable.Media_icon3, ResourceTable.Media_icon4,ResourceTable.Media_icon5};
    Random random;


    private Context context;
    DanmuAdapter(Context c){
        super();
        context = c;
        random = new Random();
    }

    @Override
    public Component getView(DanmuEntity danmuEntity, Component convertView) {

        ViewHolder1 holder1 = null;
        ViewHolder2 holder2 = null;

        if(convertView == null){
            switch (danmuEntity.getType()) {
                case 0:
                    convertView = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_item_danmu,null,false);
                    holder1 = new ViewHolder1();
                    holder1.content = (Text) convertView.findComponentById(ResourceTable.Id_content);
                    holder1.image = (Image) convertView.findComponentById(ResourceTable.Id_image);
                    convertView.setTag(holder1);
                    break;
                case 1:
                    convertView = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_item_super_danmu,null,false);
                    holder2 = new ViewHolder2();
                    holder2.content = (Text) convertView.findComponentById(ResourceTable.Id_content);
                    holder2.time = (Text) convertView.findComponentById(ResourceTable.Id_time);
                    convertView.setTag(holder2);
                    break;
            }
        }
        else{
            switch (danmuEntity.getType()) {
                case 0:
                    holder1 = (ViewHolder1)convertView.getTag();
                    break;
                case 1:
                    holder2 = (ViewHolder2)convertView.getTag();
                    break;
            }
        }

        switch (danmuEntity.getType()) {
            case 0:
                Glide.with(context)
                        .load(ICON_RESOURCES[random.nextInt(5)])
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .into(holder1.image);
                holder1.content.setText(danmuEntity.content);
                holder1.content.setTextColor(new Color(Color.rgb(random.nextInt(256), random.nextInt(256), random.nextInt(256))));
                break;
            case 1:
                holder2.content.setText(danmuEntity.content);
                holder2.time.setText(danmuEntity.getTime());
                break;
        }

        return convertView;
    }

    @Override
    public int[] getViewTypeArray() {
        int type[] = {0,1};
        return type;
    }

    @Override
    public int getSingleLineHeight() {
        //将所有类型弹幕的布局拿出来，找到高度最大值，作为弹道高度
        Component view = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_item_danmu,null,false);
        //指定行高
        view.estimateSize(0, 0);

        Component view2 = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_item_super_danmu,null,false);
        //指定行高
        view2.estimateSize(0, 0);

//        return Math.max(view.getMeasuredHeight(),view2.getMeasuredHeight());
        return view.getEstimatedHeight();
    }


    class ViewHolder1{
        public Text content;
        public Image image;
    }

    class ViewHolder2{
        public Text content;
        public Text time;
    }


}
