package com.orzangleli.recyclerviewdanmu;

import com.orzangleli.recyclerviewdanmu.slice.MainAbilitySlice;
import com.orzangleli.xdanmuku.DanmuContainerView;
import com.orzangleli.xdanmuku.Model;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Slider;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.ToastDialog;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainAbility extends Ability {
    DanmuContainerView danmuContainerView;
    Button button;
//    public String SEED[] = {"桃树、杏树、梨树，你不让我", "，都开满了花赶趟儿。红的像火，", "花里带着甜味儿，闭了眼，树上", "满是桃儿、杏儿、梨儿!花下成", "嗡地闹着，大小的蝴蝶"};
    Random random;
//    final int ICON_RESOURCES[] = {R.drawable.icon1, R.drawable.icon2, R.drawable.icon3, R.drawable.icon4, R.drawable.icon5};

    // 模拟视频时长为100秒
    private final int VIDEO_DURATION = 100 * 1000;

    private Slider mSeekBar;

    private Text mProgressTv;

    private int mCurrentProgress;

    private PlayThread mPlayThread;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
//        super.setMainRoute(MainAbilitySlice.class.getName());
        super.setUIContent(ResourceTable.Layout_ability_main);


        random = new Random();
        danmuContainerView = (DanmuContainerView) findComponentById(ResourceTable.Id_danmuContainerView);
        button = (Button) findComponentById(ResourceTable.Id_button);
        mSeekBar = (Slider) findComponentById(ResourceTable.Id_progress);
        mProgressTv = (Text) findComponentById(ResourceTable.Id_progressTv);

        mSeekBar.setMaxValue(VIDEO_DURATION);
        mProgressTv.setText("0/" + VIDEO_DURATION);


        DanmuAdapter danmuAdapter = new DanmuAdapter(this);
        danmuContainerView.setAdapter(danmuAdapter);

        danmuContainerView.setSpeed(DanmuContainerView.NORMAL_SPEED);

        danmuContainerView.setGravity(DanmuContainerView.GRAVITY_FULL);

        //弹幕点击事件
        danmuContainerView.setOnItemClickListener(new DanmuContainerView.OnItemClickListener() {
            @Override
            public void onItemClick(Model model) {
                DanmuEntity danmuEntity = (DanmuEntity) model;
                //弹框提示会造成卡顿
//                new ToastDialog(MainAbility.this).setContentText( danmuEntity.content).show();
            }
        });
        button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                List<Model> danmuEntities = new ArrayList<>();
                for (int i = 0; i < 200; i++) {
                    DanmuEntity danmuEntity = new DanmuEntity();
                    danmuEntity.setContent("弹幕"+i);
                    danmuEntity.setType(0);
                    danmuEntity.setTime("23:20:11");
                    danmuEntity.setShowTime((long) (VIDEO_DURATION * random.nextFloat()));
                    danmuEntities.add(danmuEntity);
//                    danmuContainerView.addDanmu(danmuEntity);
                }
                danmuContainerView.addDanmuIntoCachePool(danmuEntities);
                simulatePlayVideo();
                button.setEnabled(false);
            }
        });
        mSeekBar.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int progress, boolean fromUser) {
                if (fromUser) {
                    danmuContainerView.resetDanmuProgress();
                } else {
                    danmuContainerView.onProgress(progress);
                }
                mProgressTv.setText(progress + "/" + VIDEO_DURATION);
            }

            @Override
            public void onTouchStart(Slider slider) {
                if(mPlayThread!=null){
                    mPlayThread.setPause(true);
                }
            }

            @Override
            public void onTouchEnd(Slider slider) {
                if(mPlayThread!=null){
                    mPlayThread.setPause(false);
                }
            }
        });
    }
    public void simulatePlayVideo() {
        mPlayThread = new PlayThread();
        mPlayThread.start();
    }
    private class PlayThread extends Thread {
        private boolean pause;

        public void setPause(boolean pause) {
            this.pause = pause;
        }

        @Override
        public void run() {
            super.run();
            while ((mCurrentProgress = mSeekBar.getProgress()) < VIDEO_DURATION) {
                if (!pause) {
                    mCurrentProgress += 10;
                    mCurrentProgress = mCurrentProgress > VIDEO_DURATION ? VIDEO_DURATION : mCurrentProgress;
                    getUITaskDispatcher().asyncDispatch(new Runnable() {
                        @Override
                        public void run() {
                            mSeekBar.setProgressValue(mCurrentProgress);
                        }
                    });
                }
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }
    }
}
